# Class: maven
#
# This module manages maven
#
# Parameters:
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# [Remember: No empty lines between comments and class definition]
class maven ( $ensure = 'installed' ) {
  package { 'maven':
    ensure => $ensure,
  }
  
  file { '/usr/bin/maven':
    mode  => '0700',
    require => Package['maven'],
  }
}
