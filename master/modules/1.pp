package { 'apache2':
        ensure  => present,
        notify  => Service[apache2],
    }

service {'apache2': ensure => running,}
