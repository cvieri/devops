# Class: jenkins
#
# This module manages jenkins
#
# Parameters:
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# [Remember: No empty lines between comments and class definition]
class jenkins {
  package { 'jenkins': 
    ensure => installed,
  }

  service { 'jenkins':
    ensure      => running,
    require     => Package['jenkins'], 
  }
  
  file { '/etc/default/jenkins':
    ensure  => present,
    source  => 'puppet:///modules/jenkins/jenkins',
    require     => Package['jenkins'],
    notify => Service['jenkins'],
   }
}
