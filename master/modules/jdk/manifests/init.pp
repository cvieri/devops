# Class: jdk
#
# This module manages jdk
#
# Parameters:
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# [Remember: No empty lines between comments and class definition]
class jdk {
  package { 'openjdk-6-jdk':
    ensure => installed,
  }
}
