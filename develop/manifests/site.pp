node default {

  $user = notroot

  user { $user:
    ensure => present,
    shell => "/bin/bash",
    password => '$6$4BfkNR39$sVJ7FSRErM9BHsDfUZs/AN.V1pJOjiJ03UHstJ54F0HiS6v7XJodxxamoI7igiw08c1I3XhCfAJybBoKp0Jzb1',
  }
->
  class { 'sudo': }
    sudo::conf { $user:
    priority => 10,
    content  => "%${user} ALL=(ALL) NOPASSWD: ALL\n",
  }
->
  sudo::conf { 'jenkins':
    priority => 10,
    content  => "%jenkins ALL=(ALL) NOPASSWD: ALL\n",
  }
->
  class {
    'apt':;
    'percona':
      server => true,
      percona_version => '5.5',
      manage_repo => true,
  }

 ## Creation of databases
  percona::database{'drupal':
    ensure => present,
  }

  percona::rights {'drupal@localhost':
    database => 'drupal',
    password => 'drupal',
  }
->
  class { 'java':
    package => 'openjdk-6-jdk',
  }
->
  class { 'apache':
    mpm_module => prefork,
   }

  class { 'apache::mod::php': }   
->
  class { 'tomcat':
    version => 'tomcat7',
  }    
->
  class { 'jenkins': }
->
  class { 'maven': }
->
  class { 'git': 
    rsa_key => '-----BEGIN RSA PRIVATE KEY-----
MIIEpAIBAAKCAQEAoYq3scgDLh2xNM/hcNqRtJzB2WDVrXIIvm2LrVlcnTUPYalM
hRfGgzBS4HeCrc2kHENEqo4Mk6AThRXpYy+cZeZ7T+6G8AYqcEbm/5jbdPyFHR4C
ZfctnTe0VlPFJFj8tqaSTMHUUeM+75GnIad9hOW4VcS9kCrbu0ExlfGIaQ3XDtIB
NJLaMHuhBqAth0Di55j7CFkIwNukBJVmVePm+X9d8mlA62vWbPm44qV5XxyMWo7u
aaWkpszyBmlhXD5ELG8Bdm9ERk6vC0wJV+DofZd9dLQ12iqC6OGmioKmzv4yY2h/
mq7JEW3iLUCCmRigkJI5bdU3KBpKOxgErxHxSwIDAQABAoIBAEec1DRW47oE5bLw
2RMLgf5cY783I2LBo8ivjXUqoVj71bbIs6ho0tMDkCn8MQ1VlYQF+iHV2RN0qlKc
VRtoVuABnfbgWid7xEKD3BHBNMCW1XgD/rIvMqR4tlRyzHmBjX53N3IZl0M6B4Ny
wXG063sHz4EON676oPBRRWcgbQWMrYqv1GDdGdFgcvy7oWUMMaZbWtpBQ0x9PpZf
er/hzflfZHjqEwNVIy0KLy26LSjHB+Hna4AgAKUL5ZD11/pfKSg4WiRMb7jdHavH
1C3DCvSLSjxDfyDyLSL3Fo4f6z7bys+PAiD8j9Rlx46qarzyNZTGaS7+I9DBiiwK
X1kaOFECgYEA1z/uSCE5gTVKj0l//anV0qRkUUugiGw4mCNRCaqoBB6dpyUCrDeM
IPFj2UWC3Z/idzTajlYAcfgwtr6IjOriomjHCLCugWfJgYu3zyP/ChHPHo/6Bv4B
1KtxOj9lc4OkT08na193dj/riZZljMxV3TAHmPF+YmJiGueU0rmjoIkCgYEAwB/W
XTo2VWQHQW8Ibhpuu1UlVoinE3wLvmdpluRYVvZciee3PefsBdwKpC5fKXHgxb0g
k6VCahsZ7kdLBsElCkPeFrVJvEsPvg04HMslpjUYsrZUEDWy0UgyLJNhtNV6xj6u
33NZFPiQqkSOaUep78T+E0cz/gLQrnHB67EXxjMCgYEAqEhmHWREVfz49r8q3oF6
Fv3JJDmxHxckw+TQhXMzek0JSlTIGyYuWs7+Fi6fGm/WmQXsXN1l6bR1OVoK3ckB
OKIcAXJIbAldY/d80+g7i37S4/0CJ5dMjR0gH77SrRXdTtf+ZJa1ifB9UAsSB5r1
AkF7nxomifqK4qftFTZnc6kCgYEAtyRiSwJ30tSEmhqG1bY6wjb7Xc7sPYWtCDc6
D03i7Brywb7yDYAsnLVSK4nMyww0MSsFh9ksQ0tMX7j0Sx8I+Q0TA2CTRnvjKM8f
I0t/uz24KnvmDIGz0Q5aTeW8aQ5rd9k/B+DEFVqRRM4PV0qqMtb9QFFKvaPfW7sz
GUL6n8MCgYBtiBYSRDOScNlB8kMbtySd9AJfUIYNtSUL8YHUBCuaoe+PFGy8tqUw
9OAZqXdoh2UzWry8ligJP5WAZuuIFINi0LYFVx9LX8XP6F4OAL+c8K3XYgcZ6sJ9
HbGmKKbJq1c5kmlACRIOpJuG7ZSuTKGAICKpjCDZt+JA8gkw/KuTKg==
-----END RSA PRIVATE KEY-----'
  }
->
  class { 'drupal' : }

  file { ['/var/music','/var/musicpool']:
    ensure => directory,
    mode => '0777',
  }
}
