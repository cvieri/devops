node default {

$user = tim

user { $user:
        ensure => present,
        shell => "/bin/bash",
        password => '$6$4BfkNR39$sVJ7FSRErM9BHsDfUZs/AN.V1pJOjiJ03UHstJ54F0HiS6v7XJodxxamoI7igiw08c1I3XhCfAJybBoKp0Jzb1',
}

    class { 'sudo': }
    sudo::conf { $user:
      priority => 10,
      content  => "%${user} ALL=(ALL) NOPASSWD: ALL\n",
    }
}
