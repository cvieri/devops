# Class: git
#
# This module manages git
#
# Parameters:
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# [Remember: No empty lines between comments and class definition]
class git ( $rsa_key = '' ) {
  package { 'git':
    ensure => installed,
  }

  file { '/var/lib/jenkins/.ssh':
    ensure => directory,
    owner  => 'jenkins',
    group  => 'jenkins',
    mode   => '0700',
  }

  file { '/root/.ssh':
    ensure => directory,
    owner  => 'root',
    group  => 'root',
    mode   => '0700',
  }

  file { '/root/.ssh/id_rsa':
    ensure => present,
    owner  => 'root',
    group  => 'root',
    mode   => '0600',
    content => $rsa_key,
  }

  file { '/root/.ssh/config':
    ensure => present,
    owner  => 'root',
    group  => 'root',
    mode   => '0600',
    content => 'StrictHostKeyChecking no',
  }

  file { '/var/lib/jenkins/.ssh/id_rsa':
    ensure => present,
    owner  => 'jenkins',
    group  => 'jenkins',
    mode   => '0600',
    content => $rsa_key,
  }

  file { '/var/lib/jenkins/.ssh/config':
    ensure => present,
    owner  => 'jenkins',
    group  => 'jenkins',
    mode   => '0600',
    content => 'StrictHostKeyChecking no',
  }
}
