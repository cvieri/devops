# Class: drupal
#
# This module manages drupal
#
# Parameters:
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# [Remember: No empty lines between comments and class definition]
class drupal ( $dir = 'liverpool-drupal' ) {
  package { ['php5-gd', 'php-db', 'php5-mysql']:
    ensure => installed,
  }

  file { "/var/www/$dir":
    source  => 'puppet:///modules/drupal/liverpool-drupal',
    recurse => true,
    require => Exec['updating drupal'],
  }

  file { "/var/www/${dir}/sites/default/settings.php":
    content  => template('drupal/settings.php.erb'),
    ensure  => present,
    require => File["/var/www/$dir"],
  }

  exec { 'updating drupal':
    command => "bash -c 'cd /etc/puppet/modules/drupal/files/ ; git clone -b d $::environment git@bitbucket.org:etaroza/liverpool-drupal.git || ( cd /etc/puppet/modules/drupal/files/liverpool-drupal && git pull origin $::environment )'",
    path    => '/usr/bin/:/bin/',
  }
}
