# Class: tomcat
#
# This module manages tomcat
#
# Parameters:
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# [Remember: No empty lines between comments and class definition]
class tomcat ( $version = 'tomcat7' ) {
  package { $version:
    ensure => installed,
  }

}

